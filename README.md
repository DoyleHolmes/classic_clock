# Classic Alarm Clock

A simple alarm app

Requirements
* Android Studio / VSCode
* XCode
* Flutter SDK
* Android SDK
* Flutter & Dart Plugin for Android Studio / VSCode

Installing (for more information see https://flutter.dev/docs/get-started/install)
* Install Android Studio / VSCode
* Download Android SDK / set Android SDK path to current Android SDK folder
* Download Flutter & Dart plugin for Android Studio / VSCode
* Download Flutter SDK / set Flutter SDK path to current Flutter SDK folder
* If flutter was installed check with command "flutter doctor"
* If everything is checked then download dependencies with command "flutter pub get"

Building and Running
* Use terminal / cmd and set current directory to the top / root of project
* Open emulator (Android / iOS) / connect physical devices
* To run app just type "flutter run" in terminal
* To build apk just type "flutter build apk" in terminal

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
