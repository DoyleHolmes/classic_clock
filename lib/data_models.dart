const String columnId = 'id';
const String columnDate = 'date';
const String columnValue = 'value';

class DataModel {
  int? id;
  String? date;
  int? value;

  Map<String, Object?> toMap() {
    var map = <String, Object?>{columnDate: date, columnValue: value};
    map[columnId] = id;
    return map;
  }

  DataModel({this.id, this.date, this.value});

  DataModel.fromMap(Map<dynamic, dynamic> map) {
    id = map[columnId] as int?;
    date = map[columnDate] as String?;
    value = map[columnValue] as int?;
  }
}

List<DataModel> dummylist = [
  DataModel(id: 1, date: '2021-10-13 10:00:00', value: 5),
  DataModel(id: 2, date: '2021-10-13 10:01:00', value: 1),
  DataModel(id: 3, date: '2021-10-13 10:02:00', value: 8),
  DataModel(id: 4, date: '2021-10-13 10:03:00', value: 10),
  DataModel(id: 5, date: '2021-10-13 10:04:00', value: 20),
  DataModel(id: 6, date: '2021-10-13 10:05:00', value: 4),
  DataModel(id: 6, date: '2021-10-13 10:06:00', value: 6),
  DataModel(id: 6, date: '2021-10-13 10:07:00', value: 9),
  DataModel(id: 6, date: '2021-10-13 10:08:00', value: 2),
  DataModel(id: 6, date: '2021-10-13 10:09:00', value: 1),
  DataModel(id: 6, date: '2021-10-13 10:10:00', value: 8),
  DataModel(id: 6, date: '2021-10-13 10:11:00', value: 8),
  DataModel(id: 6, date: '2021-10-13 10:12:00', value: 14),
  DataModel(id: 6, date: '2021-10-13 10:13:00', value: 17),
  DataModel(id: 6, date: '2021-10-13 10:14:00', value: 14),
  DataModel(id: 6, date: '2021-10-13 10:15:00', value: 11),
  DataModel(id: 6, date: '2021-10-13 10:16:00', value: 12),
  DataModel(id: 6, date: '2021-10-13 10:17:00', value: 1),
  DataModel(id: 6, date: '2021-10-13 10:18:00', value: 8),
  DataModel(id: 6, date: '2021-10-13 10:19:00', value: 9),
  DataModel(id: 6, date: '2021-10-13 10:20:00', value: 4),
  DataModel(id: 6, date: '2021-10-13 10:21:00', value: 3),
];
