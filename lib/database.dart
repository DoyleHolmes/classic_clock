import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:collection/collection.dart';

import 'data_models.dart';

late Database database;

Future<dynamic> initDatabase() async {
  var databasesPath = await getDatabasesPath();
  String path = join(databasesPath, 'myDB.db');
  try {
    /// open the database
    database = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      /// When creating the db, create the table
      await db.execute(
          'CREATE TABLE notif_data (id INTEGER PRIMARY KEY, date TEXT, value INTEGER)');
    });
    return database;
  } catch (e) {
    debugPrint(e.toString());
    return false;
  }
}

insertData(String date) async {
  await database.transaction((txn) async {
    int id1 = await txn.rawInsert(
        'INSERT INTO notif_data(date, value) VALUES("$date", 0)');
    debugPrint('inserted1: $id1');
    // int id2 = await txn.rawInsert(
    //     'INSERT INTO notif_data(date, value) VALUES(?, ?)',
    //     ['11111111', 5]);
    // debugPrint('inserted2: $id2');
  });
}

updateData(String date, int value) async {
  int count = await database.rawUpdate(
      'UPDATE notif_data SET value = ? WHERE date = ?',
      [value, date]);
  debugPrint('updated: $count');
}

Future<List<Map>> getData() async {
  List<Map> list = await database.rawQuery('SELECT date FROM notif_data WHERE value = 0 ORDER BY id DESC');
  debugPrint(list.toString());
  return list;
}

Future<List<DataModel>?> getAllData() async {
  List<Map> list = await database.rawQuery('SELECT * FROM notif_data');
  debugPrint(list.toString());
  if (list.isNotEmpty) {
    return list.map((e) => DataModel.fromMap(e)).toList();
  } else {
    return null;
  }
}

deleteData(String data) async {
  int count = await database
      .rawDelete('DELETE FROM notif_data WHERE date = ?', [data]);
  assert(count == 1);
}

int daysBetween(DateTime from, DateTime to) {
  from = DateTime(from.year, from.month, from.day, from.hour, from.minute, from.second);
  to = DateTime(to.year, to.month, to.day, to.hour, to.minute, to.second);
  return (to.difference(from).inSeconds).abs();
}

