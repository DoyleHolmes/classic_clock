import 'dart:io';
import 'dart:math';

import 'package:classicclock/received_notification.dart';
import 'package:classicclock/shared.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_native_timezone/flutter_native_timezone.dart';
import 'package:intl/intl.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;

import 'package:timezone/data/latest.dart';
import 'package:timezone/timezone.dart' as t;
import 'package:flutter_native_timezone/flutter_native_timezone.dart';

import 'database.dart';
import 'main.dart';

class NotificationService {
  static final NotificationService _notificationService =
      NotificationService._internal();

  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  factory NotificationService() {
    return _notificationService;
  }

  NotificationService._internal();

  static const channel_id = "1234";
  late DateFormat formatter;

  Future<void> init() async {
    await _configureLocalTimeZone();
    AndroidInitializationSettings initializationSettingsAndroid;
    if (Platform.isAndroid) {
      initializationSettingsAndroid =
          const AndroidInitializationSettings('app_icon');
    } else {
      initializationSettingsAndroid =
          const AndroidInitializationSettings('app_icon');
    }
    final IOSInitializationSettings initializationSettingsIOS =
        IOSInitializationSettings(
            requestAlertPermission: false,
            requestBadgePermission: false,
            requestSoundPermission: false,
            onDidReceiveLocalNotification:
                (int id, String? title, String? body, String? payload) async {
              didReceiveLocalNotificationSubject.add(ReceivedNotification(
                  id: id, title: title, body: body, payload: payload));
            });

    final InitializationSettings initializationSettings =
        InitializationSettings(
            android: initializationSettingsAndroid,
            iOS: initializationSettingsIOS,
            macOS: null);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: selectNotification);
  }

  Future<void> _configureLocalTimeZone() async {
    tz.initializeTimeZones();
    final String? timeZoneName = await FlutterNativeTimezone.getLocalTimezone();
    tz.setLocalLocation(tz.getLocation(timeZoneName!));
  }

  void selectNotification(String? payload) async {
    // debugPrint('notification payload: $payload');

    formatter = DateFormat('yyyy-MM-dd HH:mm:ss');
    List<Map> list = await getData();
    debugPrint('list >>>>>> : ${formatter.format(DateTime.now())}');
    debugPrint('single >>>>>> : ${list[0]['date']}');
    final difference = daysBetween(DateTime.parse(list[0]['date']),
        DateTime.parse(formatter.format(DateTime.now())));
    debugPrint('difference >>>>>> : $difference');
    updateData(list[0]['date'], difference);
    Shared.save('notif', '1');
  }

  Future<void> zonedScheduleNotification(DateTime dateTime) async {
    final timeZone = TimeZone();

    /// The device's timezone.
    String timeZoneName = await timeZone.getTimeZoneName();

    /// Find the 'current location'
    final location = await timeZone.getLocation(timeZoneName);
    final scheduledDate = tz.TZDateTime.from(dateTime, location);

    if (scheduledDate.isBefore(DateTime.now())) {
      scheduledDate.add(const Duration(days: 1));
    }

    debugPrint('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.');
    debugPrint('$dateTime');
    debugPrint('$scheduledDate');
    debugPrint('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.');

    const AndroidNotificationDetails androidPlatformChannelSpecifics =
        AndroidNotificationDetails(
      '1412',
      'clock_channel',
      channelDescription: 'channelDescription',
      sound: RawResourceAndroidNotificationSound('slow_spring_board'),
    );
    const IOSNotificationDetails iOSPlatformChannelSpecifics =
        IOSNotificationDetails(sound: 'slow_spring_board.aiff');
    const NotificationDetails platformChannelSpecifics = NotificationDetails(
      android: androidPlatformChannelSpecifics,
      iOS: iOSPlatformChannelSpecifics,
    );
    await flutterLocalNotificationsPlugin.zonedSchedule(
        0,
        'Alarm !!!',
        'FIRED',
        scheduledDate,
        // tz.TZDateTime.now(tz.local).add(const Duration(seconds: 5)),
        platformChannelSpecifics,
        androidAllowWhileIdle: true,
        uiLocalNotificationDateInterpretation:
            UILocalNotificationDateInterpretation.absoluteTime);
  }
}

class TimeZone {
  factory TimeZone() => _this ?? TimeZone._();

  TimeZone._() {
    initializeTimeZones();
  }

  static TimeZone? _this;

  Future<String> getTimeZoneName() async =>
      FlutterNativeTimezone.getLocalTimezone();

  Future<t.Location> getLocation([String? timeZoneName]) async {
    if (timeZoneName == null || timeZoneName.isEmpty) {
      timeZoneName = await getTimeZoneName();
    }
    return t.getLocation(timeZoneName);
  }
}
