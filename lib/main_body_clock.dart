import 'dart:io';

import 'package:classicclock/database.dart';
import 'package:classicclock/notification_services.dart';
import 'package:classicclock/report_response.dart';
import 'package:classicclock/shared.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import 'flutter_analog_clock_painter.dart';
import 'main.dart';

class MainBodyClock extends StatefulWidget {
  const MainBodyClock({
    Key? key,
  }) : super(key: key);

  @override
  _MainBodyClockState createState() => _MainBodyClockState();
}

class _MainBodyClockState extends State<MainBodyClock>
    with WidgetsBindingObserver {
  int i = 0;
  int start = 0, end = 0, update = 0, starttemp = 0, starttemp1 = 0;
  int start5 = 0, end5 = 0, update5 = 0, starttemp5 = 0, starttemp15 = 0;
  late DateTime time;
  late DateFormat formatter, formatter1;
  String notif = "0";
  late BuildContext _context;
  AppLifecycleState _notification = AppLifecycleState.paused;

  /// https://youtu.be/x9yHW98VJ8M

  @override
  void initState() {
    if (Platform.isIOS) {
      _requestPermissions();
    }
    // Shared.save('notif', '1');
    time = DateTime.now();
    formatter = DateFormat('HH:mm');
    formatter1 = DateFormat('yyyy-MM-dd HH:mm');
    WidgetsBinding.instance?.addObserver(this);
    super.initState();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    notif = await Shared.read('notif') ?? '0';
    setState(() {
      _notification = state;
      if (mounted && _notification == AppLifecycleState.paused) {
        if (notif == '1') {
          Shared.save('notif', '0');
          Navigator.push(
            _context,
            MaterialPageRoute(builder: (context) => const ReportScreenPage()),
          );
        }
      }
    });
  }

  @override
  void didChangeDependencies() async {
    await NotificationService().init();
    await initDatabase();
    // dynamic db = await initDatabase();
    // if (db is int) {
    //   debugPrint('FAILED DB');
    // } else {
    //   getData();
    // }
    super.didChangeDependencies();
  }

  void _requestPermissions() {
    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          alert: true,
          badge: true,
          sound: true,
        );
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            formatter.format(DateTime.parse(time.toString())).toString(),
            style: const TextStyle(fontSize: 80, fontWeight: FontWeight.bold),
          ),
          const SizedBox(
            height: 20,
          ),
          GestureDetector(
            onTap: () {
              // NotificationService().zonedScheduleNotification();
            },
            onHorizontalDragUpdate: (details) {
              update5 = details.delta.dx.toInt();
              time = time.add(Duration(minutes: update5));
              if (mounted) {
                setState(() {});
              }
            },
            onVerticalDragUpdate: (details) {
              update = (details.delta.dy.toInt());
              if (starttemp != update) {
                starttemp = update;
                time = time.add(Duration(hours: update));
                if (mounted) {
                  setState(() {});
                }
              }
            },
            child: SizedBox(
              width: 200,
              height: 200,
              child: CustomPaint(
                painter: FlutterAnalogClockPainter(
                  time,
                  dialPlateColor: Colors.black,
                  hourHandColor: Colors.grey,
                  minuteHandColor: Colors.grey,
                  secondHandColor: Colors.grey,
                  numberColor: Colors.grey,
                  borderColor: Colors.black,
                  tickColor: Colors.grey,
                  centerPointColor: Colors.grey,
                  showBorder: true,
                  showTicks: true,
                  showMinuteHand: true,
                  showSecondHand: false,
                  showNumber: true,
                  borderWidth: 0,
                  hourNumberScale: 1.0,
                  hourNumbers: FlutterAnalogClockPainter.defaultHourNumbers,
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          InkWell(
            onTap: () async {
              // final difference = daysBetween(time, DateTime(2021, 10, 11, 22, 00, 00));
              // debugPrint(difference.toString());
              Shared.save('notif', '1');
              Shared.save('dummy', '0');
              debugPrint(formatter1.format(time) + ':00');
              insertData(formatter1.format(time) + ':00');
              NotificationService().zonedScheduleNotification(
                  DateTime.parse(formatter1.format(time) + ':00'));
              Fluttertoast.showToast(
                  msg: "Alarm set successfully",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.CENTER,
                  timeInSecForIosWeb: 1,
                  backgroundColor: const Color(0x8A000000),
                  textColor: Colors.white,
                  fontSize: 15.0);
            },
            child: const SizedBox(
              child: Text(
                'Set Alarm',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          InkWell(
            onTap: () {
              if (mounted) {
                Shared.save('dummy', '1');
                Navigator.push(
                  _context,
                  MaterialPageRoute(
                      builder: (context) => const ReportScreenPage()),
                );
              }
            },
            child: const SizedBox(
              child: Text(
                'Dummy Data 22',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(this);
    super.dispose();
  }
}
