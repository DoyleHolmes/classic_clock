import 'dart:async';

import 'package:classicclock/data_models.dart';
import 'package:classicclock/database.dart';
import 'package:classicclock/shared.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import 'flutter_analog_clock_painter.dart';
import 'main_body_clock.dart';

class ReportScreenPage extends StatefulWidget {
  const ReportScreenPage({Key? key}) : super(key: key);

  @override
  _ReportScreenPageState createState() => _ReportScreenPageState();
}

class _ReportScreenPageState extends State<ReportScreenPage>
    with WidgetsBindingObserver {
  late List<DataModel> _list;
  late TooltipBehavior _tooltipBehavior;
  AppLifecycleState _notification = AppLifecycleState.paused;

  @override
  void initState() {
    _tooltipBehavior = TooltipBehavior(enable: true);
    WidgetsBinding.instance?.addObserver(this);
    super.initState();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    _list = await getList();
    if (mounted) {
      setState(() {
        _notification = state;
        if (_notification == AppLifecycleState.paused) {
          _list;
        }
      });
    }
  }

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
  }

  Future<List<DataModel>> getList() async {
    String dummy = await Shared.read('dummy') ?? '0';
    if (dummy == '0') {
      _list = (await getAllData())!;
    } else {
      _list = dummylist;
    }
    debugPrint('LIST >>>>>>>>>>>>');
    debugPrint('LIST >>>>>>>>>>>> ${_list.length}');
    return _list;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          return redirectTo();
        },
        child: Scaffold(
          body: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: FutureBuilder(
                  future: getList(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return SfCartesianChart(
                          // Initialize category axis
                          primaryXAxis: CategoryAxis(
                              visibleMinimum: 1, visibleMaximum: 8),
                          zoomPanBehavior: ZoomPanBehavior(
                            enablePanning: true,
                            enablePinching: true,
                            zoomMode: ZoomMode.xy,
                            maximumZoomLevel: 0.01,
                          ),
                          title: ChartTitle(text: 'How long you click alarm'),
                          // Enable legend
                          // legend: Legend(isVisible: true),
                          // Enable tooltip
                          // tooltipBehavior: _tooltipBehavior,
                          series: <ColumnSeries<DataModel, String>>[
                            ColumnSeries<DataModel, String>(
                                // Bind data source
                                dataSource: _list,
                                // dataSource: _list,
                                xValueMapper: (DataModel dataModel, _) =>
                                    dataModel.date!.substring(11, 16),
                                yValueMapper: (DataModel dataModel, _) =>
                                    dataModel.value)
                          ]);
                    } else {
                      return const Text('');
                    }
                  }),
            ),
          ),
        ));
  }

  Future<bool> redirectTo() async {
    Shared.save('notif', '0');
    Navigator.pop(
      context,
      MaterialPageRoute(builder: (context) {
        return const MainBodyClock();
      }),
    );
    return true;
  }
}
